package com.example.weathermap;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;


import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.widget.Button;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    Button Pokaz;
    EditText city;
    int koll=0;

    double cityLat, cityLng;

    LatLng location;

    Marker marker;

    TextView temp_TV, feelsLike_TV, description_TV;

    LinearLayout L1, L2, L3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        city=findViewById(R.id.editText);
        temp_TV = findViewById(R.id.temp_TV);
        feelsLike_TV = findViewById(R.id.feelsLike_TV);
        description_TV = findViewById(R.id.description_TV);
        L1 = findViewById(R.id.L1);
        L2 = findViewById(R.id.L2);
        L3 = findViewById(R.id.L3);
        Pokaz = findViewById(R.id.button);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        getCityInfo(city.getText().toString());
    }





    // Клик по кнопке "Поиск"

    public void Search_Click(View view) {

        getCityInfo(city.getText().toString());

    }



    // Получение информации о городе

    private void getCityInfo(String cityName)
    {
        String url = "https://api.openweathermap.org/data/2.5/weather?q=" + cityName + "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=metric&lang=ru";
        JsonObjectRequest jo = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                        if (marker != null)
                        {
                            marker.remove();
                        }

                        JSONObject coord = response.getJSONObject("coord");

                        cityLat = Double.valueOf(coord.getDouble("lat"));
                        cityLng = Double.valueOf(coord.getDouble("lon"));

                        location = new LatLng(cityLat, cityLng);
                        marker = mMap.addMarker(new MarkerOptions().position(location).title(city.getText().toString()));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));


                        JSONObject main = response.getJSONObject("main");
                        temp_TV.setText("Температура: " + String.valueOf(main.getDouble("temp")) + "°C");
                        feelsLike_TV.setText("Ощущается как: " + String.valueOf(main.getDouble("feels_like")) + "°C");

                        JSONObject zero = response.getJSONArray("weather").getJSONObject(0);
                        description_TV.setText("Погода: " + String.valueOf(zero.getString("description")));
                    }


                catch (Exception ex)
                {

                }
            }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { }});
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jo);

        temp_TV.setText("Место не найдено");
        feelsLike_TV.setText("");
        description_TV.setText("");
    }

    public void Pokaz(View view)
    {
        koll++;


        if(koll%2==1)
        {
            Pokaz.setText("Показать");
            L1.setPadding(0,0,0,-80);
            L2.setPadding(0,0,0,-80);
            L3.setPadding(0,0,0,-80);
        }
        else
        {
            Pokaz.setText("Скрыть");
            L1.setPadding(0,0,0,0);
            L2.setPadding(0,0,0,0);
            L3.setPadding(0,0,0,0);
        }

    }
}
